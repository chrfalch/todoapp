﻿using System;
using System.Linq;

namespace CF.Todo.DataLayer
{
	public interface IRepository<T> where T : IModel
	{
		IQueryable<T> Items(); 
		void Add(T entity);
		void Remove(T entity);
		void Update(T entity);
		int Count { get; }
		T GetById(string id);
	}
}

