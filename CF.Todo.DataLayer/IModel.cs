﻿using System;

namespace CF.Todo.DataLayer
{
	public interface IModel
	{
		string Id { get; set; }
	}
}

