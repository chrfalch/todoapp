﻿using System;
using CF.Todo.DataLayer;
using System.Collections.Generic;
using System.Linq;

namespace CF.Todo.DataLayer
{
	public class Repository<TModel>: IRepository<TModel>
		where TModel : IModel
	{
		private readonly List<TModel> _list = new List<TModel>();

		public Repository ()
		{
		}

		public Repository (IEnumerable<TModel> items)
		{
			_list.AddRange (items);
		}

		public System.Linq.IQueryable<TModel> Items ()
		{
			return _list.AsQueryable ();
		}

		public void Add (TModel entity)
		{
			_list.Add (entity);
		}

		public void Remove (TModel entity)
		{
			_list.Remove (entity);
		}

		public void Update (TModel entity)
		{
			// No need to update, we don't save stuff here
		}

		public TModel GetById (string id)
		{
			return _list.SingleOrDefault (mn => mn.Id == id);
		}

		public int Count {
			get {
				return _list.Count;
			}
		}
	}
}

