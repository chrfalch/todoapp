﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace CF.Todo.DataLayer
{
	public class BaseModel: IModel, INotifyPropertyChanged
	{
		#region INotifyPropertyChanged implementation
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region IModel implementation

		string _id;
		public string Id {
			get {
				return _id;
			}
			set {
				if (_id == value)
					return;

				_id = value;
				RaisePropertyChanged<BaseModel> (m => m.Id);
			}
		}
		#endregion

		/// <summary>
		/// Raises the property changed event.
		/// </summary>
		/// <param name="property">Property.</param>
		/// <typeparam name="TModel">The 1st type parameter.</typeparam>
		protected void RaisePropertyChanged<T>(Expression<Func<T, object>> property) 
		{
			if (PropertyChanged == null)
				return;

			string propertyName = string.Empty;

			if (property.Body.NodeType == ExpressionType.MemberAccess)
			{
				var memberExpression = property.Body as MemberExpression;
				if (memberExpression != null)
					propertyName = memberExpression.Member.Name;
			}
			else
			{
				var unary = property.Body as UnaryExpression;
				if (unary != null)
				{
					var member = unary.Operand as MemberExpression;
					if (member != null) propertyName = member.Member.Name;
				}
			}

			PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
		}
	}
}

