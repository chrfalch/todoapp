﻿using System;

namespace CF.Todo.DataLayer
{
	public class TodoModel: BaseModel, IModel
	{
		public TodoModel ()
		{
		}

		DateTime _timeStamp;
		public DateTime TimeStamp {
			get {
				return _timeStamp;
			}
			set {
				if (_timeStamp == value)
					return;

				_timeStamp = value;
				RaisePropertyChanged<TodoModel> (m => m.Done);

			}
		}

		String _text;
		public String Text {
			get {
				return _text;
			}
			set {
				if (_text == value)
					return;

				_text = value;
				RaisePropertyChanged<TodoModel> (m => m.Text);
			}
		}

		bool _done;
		public bool Done {
			get {
				return _done;
			}
			set {
				if (_done == value)
					return;

				_done = value;
				RaisePropertyChanged<TodoModel> (m => m.Done);
			}
		}
			

	}
}

