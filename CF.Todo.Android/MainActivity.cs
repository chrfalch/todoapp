﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;
using CF.Todo.App;
using Xamarin.Forms.Platform.Android;
using CF.Todo.DataLayer;

namespace CF.Todo.Android
{
	[Activity (Label = "CF.Todo.Android", MainLauncher = true)]
	public class MainActivity : AndroidActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Forms.Init(this, bundle);

			SetPage (TodoApp.GetStartPage());
		}
	}
}


