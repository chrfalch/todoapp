﻿using System;
using Xamarin.Forms;
using CF.Todo.DataLayer;

namespace CF.Todo.App
{
	public class TodoListView: ContentPage
	{
		private readonly TodoListViewModel _viewModel;

		public TodoListView (IRepository<TodoModel> repository)
		{
			// Create viewmodel
			_viewModel = new TodoListViewModel (repository);

			Title = "TodoApp";

			// Create list view
			var list = new ListView ();

			// Set items source
			list.ItemsSource = _viewModel.Items;

			// Create item template
			var cell = new DataTemplate (typeof (TextCell));
			cell.SetBinding (TextCell.TextProperty, "Text");
			cell.SetBinding (TextCell.DetailProperty, new Binding("TimeStamp", BindingMode.OneWay, 
				null, null, "{0:d}"));

			list.ItemTemplate = cell;

			// Hook up on tapped
			list.ItemTapped += (object sender, ItemTappedEventArgs e) => {
				Navigation.PushAsync(new TodoDetailsView(e.Item as TodoModel));
			};
				
			// Set page content
			Content = list;

			// Add toolbar button
			ToolbarItems.Add (new ToolbarItem ("Add", "plus.png", _viewModel.AddTodoItem));
		}
	}
}

