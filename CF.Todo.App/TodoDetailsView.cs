﻿using System;
using Xamarin.Forms;
using CF.Todo.DataLayer;

namespace CF.Todo.App
{
	public class TodoDetailsView: ContentPage
	{
		public TodoDetailsView (TodoModel model)
		{

			// Create viewmodel 
			BindingContext = new TodoDetailsViewModel (model);

			Title = "Todo Item Details";

			var titleCell = new EntryCell {Label = "Title"};
			titleCell.SetBinding(EntryCell.TextProperty, "TodoItem.Text");

			var switchCell = new SwitchCell{ Text = "Done/Not done" };
			switchCell.SetBinding (SwitchCell.OnProperty, "TodoItem.Done");

			Content = new TableView {
				Root = new TableRoot () {
					new TableSection ("Todo Item Details") { titleCell, switchCell },
				}
			};
		}
	}
}

