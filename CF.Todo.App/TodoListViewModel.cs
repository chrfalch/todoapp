﻿using System;
using CF.Todo.DataLayer;
using System.Collections.ObjectModel;
using System.Linq;

namespace CF.Todo.App
{
	public class TodoListViewModel
	{
		private readonly IRepository<TodoModel> _repository;
		private ObservableCollection<TodoModel> _items;

		public TodoListViewModel (IRepository<TodoModel> repository)
		{
			_repository = repository;
		}


		public ObservableCollection<TodoModel> Items {
			get {			

				if (_items == null)
					_items = new ObservableCollection<TodoModel> (_repository.Items().ToList());

				return _items;
			}
		}

		public void AddTodoItem(){

			// Create new model
			var newModel = new TodoModel{ Text = "Hello World!", TimeStamp = DateTime.Now };

			// Add to repository
			_repository.Add (newModel);

			// Update ViewModel
			_items.Add (newModel);
		}
			
	}
}

