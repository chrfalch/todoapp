﻿using System;
using CF.Todo.DataLayer;

namespace CF.Todo.App
{
	public class TodoDetailsViewModel
	{
		private readonly TodoModel _model;

		public TodoDetailsViewModel (TodoModel model)
		{
			_model = model;
		}

		public TodoModel TodoItem{
			get{ return _model; }
		}
	}
}

