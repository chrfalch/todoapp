﻿using System;
using Xamarin.Forms;
using CF.Todo.DataLayer;

namespace CF.Todo.App
{
	public static class TodoApp
	{
		public static Page GetStartPage(){
			return new NavigationPage (new TodoListView(new Repository<TodoModel>(new []{
				new TodoModel{Text = "Kjøp melk", TimeStamp = DateTime.Now},
				new TodoModel{Text = "Xamarin Meetup", TimeStamp = DateTime.Now}
			})));				
		}
	}
}

